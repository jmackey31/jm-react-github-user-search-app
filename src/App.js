import React from 'react';

import './index.scss'

const App = ({children}) => {
    return (
        <div className="app-wrapper">
            {children}
        </div>
    );
};

export default App;
