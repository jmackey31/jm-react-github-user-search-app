import React from 'react';
import {Link} from 'react-router-dom';

const App404 = () => {
    return (
        <div>
            <h1>Page Not Found</h1>
            <Link to="/">
                leave this page
            </Link>
        </div>
    );
};

export default App404;
