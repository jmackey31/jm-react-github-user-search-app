import React, { useState } from 'react';

import axios from 'axios';

import './Home.scss';

import JMPagination from '../JMPagination/JMPagination';

const Home = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [users, setUsers] = useState([]);
    const [resultsCount, setResultsCount] = useState(0);
    const [page, setPage] = useState(1);
    const [pageRange, setPageRange] = useState([]);
    const limit = 5;
    const [selectedUser, setSelectedUser] = useState({});
    const pagination = {
        limit,
        page,
        pages: resultsCount,
        page_range: pageRange,
        total: resultsCount
    };

    const handleSelectedUser = user => {
        setSelectedUser(user);
    };

    const dataRecordFetch = async page_arg => {
        try {
            const url = `https://api.github.com/search/users?q=${searchTerm}&page=${page_arg}&per_page=${limit}`;
            const response = await axios({
                method: 'GET', url
            }).then(data_response => data_response.data).catch(e => e);
            const { items, total_count } = response;
            let new_page_range = [];

            if (total_count > 0) {
                setUsers(items);
                setResultsCount(total_count);

                // Create the page range.
                for (let i = 1; i < total_count + 1; i++) {
                    new_page_range.push(i);
                }

                setPageRange(new_page_range);
            } else {
                throw {
                    success: false,
                    message: 'There were no users found.'
                };
            }
        } catch (e) {
            if (e.message) {
                alert(e.message);
            } else {
                alert('Could not complete this action.  There was a problem.');
            }
        }
    };

    const submitSearchTerm = event => {
        event.preventDefault();

        dataRecordFetch(1).then(response => response);
    };

    const renderUsersList = () => {
        return users.map(user => {
            const { avatar_url, id, login, repos_url } = user;

            return <div
                className="app-wrapper-user-thumb-block"
                key={id}
                onClick={() => handleSelectedUser(user)}>
                <img
                    src={avatar_url}
                    alt={login}
                />
                <div className="app-wrapper-thumb-info-block">
                    <h4>Username: {login}</h4>
                    <span>Repos: {repos_url}</span>
                </div>
            </div>;
        });
    };

    const renderSelectedUser = () => {
        const {
            avatar_url, events_url, followers_url, gists_url, id, login, repos_url,
        } = selectedUser;

        if (id && login) {
            return <div className="app-wrapper-selected-user-wrapper">
                <img
                    src={avatar_url}
                    alt={login}
                />
                <h1>{login}</h1>
                <ul>
                    <li>Events URL: {events_url}</li>
                    <li>Followers URL: {followers_url}</li>
                    <li>GISTS URL: {gists_url}</li>
                    <li>Repos URL: {repos_url}</li>
                </ul>
            </div>;
        }
    }

    return (
        <div className="app-wrapper-inner">
            <h1>JM GitHub User Search App</h1>
            <hr/>
            <form className="app-wrapper-form" onSubmit={submitSearchTerm} noValidate>
                <div className="app-wrapper-form-input-wrapper">
                    <input
                        type="text"
                        value={searchTerm}
                        onChange={e => setSearchTerm(e.target.value)}
                    />
                    <button type="submit">Search Users</button>
                </div>
            </form>
            <div className="app-wrapper-lower-block">
                <div className="app-wrapper-results-block">
                    {resultsCount > 0 && <p>results ({resultsCount})</p>}
                    {renderUsersList()}
                    <div className="clear-block">-</div>
                </div>
                <div className="app-wrapper-selected-user-block">
                    {renderSelectedUser()}
                    <div className="clear-block">-</div>
                </div>
            </div>
            <div className="app-wrapper-lower-pagination-block">
                <JMPagination
                    pagination={pagination}
                    dataRecordFetch={dataRecordFetch}
                    setPage={setPage}
                />
                <div className="clear-block">-</div>
            </div>
        </div>
    );
};

export default Home;
