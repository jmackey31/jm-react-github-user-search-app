import React from 'react';

const JMPagination = ({ pagination, dataRecordFetch, setPage }) => {
    const { page, pages, page_range, total } = pagination;

    const previousPagination = () => {
        if (page <= 1) {
            alert('You are at the very beginning.');
        } else {
            const new_page_value = Number(page) - 1;
            setPage(new_page_value);
            dataRecordFetch(new_page_value);
        }
    };

    const nextPagination = () => {
        if (page >= Number(page_range.length)) {
            alert('You have reached the end.');
        } else {
            const new_page_value = Number(page) + 1;
            setPage(new_page_value);
            dataRecordFetch(new_page_value);
        }
    };

    const onRenderPaginationButtons = () => {
        return (
            <div className="app-wrapper-pagination-wrapper">
                <p>({total}) Results | Page "{page}" of "{pages}"</p>
                <ul className="app-wrapper-pagination-ul">
                    <li className="app-wrapper-pagination-li">
                        <button onClick={() => previousPagination()}>back</button>
                    </li>
                    <li className="btn-search-pagination-li">
                        <button onClick={() => nextPagination()}>next</button>
                    </li>
                </ul>
            </div>
        );
    };

    return <div>{pages > 1 && onRenderPaginationButtons()}</div>;
};

export default JMPagination;
